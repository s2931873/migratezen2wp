var express = require('express');
var router = express.Router();
var axios = require('axios');
const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'oob_migration';

// Create a new MongoClient
const client = new MongoClient(url);

let db = null;

async function insertMany(data, collectionName) {
  const collection = db.collection(collectionName);
  // Insert some documents
  await collection.insertMany(data);
}

function requestProducts(){
  return axios.get('http://127.0.0.1:80/?products');
}

async function getProducts() {
  return await requestProducts().then((data)=>{
    return data.data;
  });
}

/* GET home page. */
router.get('/', async function(req, res, next) {
  client.connect(function(err) {
    console.log("Connected successfully to server");

    db = client.db(dbName);
  });

  await getProducts().then(async (data)=>{
      await insertMany(data, 'products');
      return res.send(data);
  });
});

module.exports = router;
